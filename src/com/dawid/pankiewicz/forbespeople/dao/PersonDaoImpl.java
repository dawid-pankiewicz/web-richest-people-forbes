package com.dawid.pankiewicz.forbespeople.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dawid.pankiewicz.forbespeople.entity.Person;

@Repository
public class PersonDaoImpl implements PersonDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Person> getPersons() {

		Session currentSession = sessionFactory.getCurrentSession();
		Query<Person> query = currentSession.createQuery("from Person order by netWorth desc", Person.class);
		List<Person> persons = query.getResultList();

		return persons;
	}

	@Override
	public void savePerson(Person person) {

		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.saveOrUpdate(person);

	}

	@Override
	public Person getPerson(int id) {

		Session currentSession = sessionFactory.getCurrentSession();
		Person person = currentSession.get(Person.class, id);

		return person;
	}

	@Override
	public void deletePerson(int id) {

		Session currentSession = sessionFactory.getCurrentSession();
		Person person = getPerson(id);
		
		currentSession.delete(person);

	}

}
