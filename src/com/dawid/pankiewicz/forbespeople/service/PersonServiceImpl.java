package com.dawid.pankiewicz.forbespeople.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dawid.pankiewicz.forbespeople.dao.PersonDAO;
import com.dawid.pankiewicz.forbespeople.entity.Person;

@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonDAO personDAO;

	@Override
	@Transactional
	public List<Person> getPersons() {

		return personDAO.getPersons();
	}

	@Override
	@Transactional
	public void savePerson(Person person) {
		personDAO.savePerson(person);

	}

	@Override
	@Transactional
	public Person getPerson(int id) {

		return personDAO.getPerson(id);
	}

	@Override
	@Transactional
	public void deletePerson(int id) {

		personDAO.deletePerson(id);
	}

}
