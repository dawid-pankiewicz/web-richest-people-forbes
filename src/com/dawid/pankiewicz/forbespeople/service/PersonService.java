package com.dawid.pankiewicz.forbespeople.service;

import java.util.List;

import com.dawid.pankiewicz.forbespeople.entity.Person;

public interface PersonService {

	public List<Person> getPersons();

	public void savePerson(Person person);

	public Person getPerson(int id);

	public void deletePerson(int id);

}
