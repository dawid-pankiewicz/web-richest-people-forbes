package com.dawid.pankiewicz.forbespeople.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dawid.pankiewicz.forbespeople.entity.Person;
import com.dawid.pankiewicz.forbespeople.service.PersonService;

@RestController
@RequestMapping("/rest")
public class PersonRestController {

	@Autowired
	private PersonService personService;

	// http://localhost:8080/web-richest-people-forbes/rest/persons
	@GetMapping("/persons")
	public List<Person> getPersons() {

		return personService.getPersons();

	}

	// http://localhost:8080/web-richest-people-forbes/rest/persons/{id}
	@GetMapping("/persons/{id}")
	public ResponseEntity<?> getPerson(@PathVariable("id") int id) {

		Person person = personService.getPerson(id);

		if (person == null) {
			return new ResponseEntity("No Person found for ID " + id, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity(person, HttpStatus.OK);
	}

	// http://localhost:8080/web-richest-people-forbes/rest/persons
	// Method: POST
	// Type: JSON

	// ### Pareameters ###
	// firstName -> exampleFirstName
	// lastName -> exampleLastName
	// sourceOfIncome -> exampleSourceOfIncome
	// age -> exampleFirstName
	// netWorth -> exampleNetWorth
	// countryOfCitizenship -> exampleCountryOfCitizenship
	@PostMapping("/persons")
	public ResponseEntity<?> createPerson(@RequestBody Person person) {

		personService.savePerson(person);

		return new ResponseEntity<Object>(person, HttpStatus.OK);
	}

	// http://localhost:8080/web-richest-people-forbes/rest/persons/{id}
	// Method: DELETE
	@DeleteMapping("/persons/{id}")
	public ResponseEntity<?> deletePerson(@PathVariable int id) {

		Person person = personService.getPerson(id);

		if (null == person) {
			return new ResponseEntity("No Person found for ID " + id, HttpStatus.NOT_FOUND);
		} else {
			personService.deletePerson(id);
			return new ResponseEntity(person + " was deleted", HttpStatus.OK);
		}

	}

	// http://localhost:8080/web-richest-people-forbes/rest/persons/{id}
	// Method: PUT
	// Type: JSON

	// ### Pareameters ###
	// firstName -> exampleFirstName
	// lastName -> exampleLastName
	// sourceOfIncome -> exampleSourceOfIncome
	// age -> exampleFirstName
	// netWorth -> exampleNetWorth
	// countryOfCitizenship -> exampleCountryOfCitizenship
	@PutMapping("/persons/{id}")
	public ResponseEntity<?> updatePerson(@PathVariable int id, @RequestBody Person person) {

		Person personFromDb = personService.getPerson(id);

		if (null == personFromDb) {
			return new ResponseEntity("No Person found", HttpStatus.NOT_FOUND);
		} else {

			personFromDb.setFirstName(person.getFirstName());
			personFromDb.setLastName(person.getLastName());
			personFromDb.setSourceOfIncome(person.getSourceOfIncome());
			personFromDb.setAge(person.getAge());
			personFromDb.setNetWorth(person.getNetWorth());
			personFromDb.setCountryOfCitizenship(person.getCountryOfCitizenship());

			personService.savePerson(personFromDb);

			return new ResponseEntity<Object>(personFromDb, HttpStatus.OK);
		}
	}
}
