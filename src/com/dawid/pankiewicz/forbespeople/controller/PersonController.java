package com.dawid.pankiewicz.forbespeople.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.dawid.pankiewicz.forbespeople.entity.Person;
import com.dawid.pankiewicz.forbespeople.service.PersonService;

@Controller
@RequestMapping("/people")
public class PersonController {

	@Autowired
	private PersonService personService;

	@GetMapping("/list")
	public String listPersons(Model model) {

		List<Person> persons = personService.getPersons();

		model.addAttribute("persons", persons);

		return "list-persons";
	}

	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model model) {

		Person person = new Person();

		model.addAttribute("person", person);

		return "person-form";
	}

	@PostMapping("/savePerson")
	public String savePerson(@ModelAttribute("person") Person person) {

		personService.savePerson(person);

		return "redirect:/people/list";
	}

	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("personId") int Id, Model model) {

		Person person = personService.getPerson(Id);

		model.addAttribute("person", person);

		return "person-form";
	}

	@GetMapping("/delete")
	public String deletePersonById(@RequestParam("personId") int Id) {

		personService.deletePerson(Id);

		return "redirect:/people/list";
	}
	

}
