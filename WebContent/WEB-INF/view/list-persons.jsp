<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>The World's Billionaires</title>
	
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css" rel="stylesheet" />
	<script	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js"></script>
	<link type="text/css" rel="stylesheet"	href="${pageContext.request.contextPath}/resources/css/style.css">
</head>
<body>

	<div class="jumbotron text-center header">
		<h2>Forbes - The World's Billionaires</h2>
	</div>

	<input type="button" value="Add Person" class="btn btn-block add-button" onclick="window.location.href='showFormForAdd'; return false;" >
	<div class="table-responsive">
		<table data-toggle="table" data-sort-name="net_worth" data-sort-order="desc">
			<thead>
				<tr>
					<th data-field="first_name" data-sortable="true">First Name</th>
					<th data-field="last_name" data-sortable="true">Last Name</th>
					<th data-field="source_of_income" data-sortable="true">Source Of Income</th>
					<th data-field="age" data-sortable="true">Age</th>
					<th data-field="net_worth" data-sortable="true">Net Worth<br>[in miliards <br> Dollars]</th>
					<th data-field="country_of_citizenship" data-sortable="true">Country Of Citizenship</th>
					<th data-field="action">Action</th>
				</tr>
			</thead>
			<tbody>
			
				<c:forEach var="tempPersons" items="${persons}">

					<c:url var="updateLink" value="/people/showFormForUpdate">
						<c:param name="personId" value="${tempPersons.id}" />
					</c:url>
					<c:url var="deleteLink" value="/people/delete">
						<c:param name="personId" value="${tempPersons.id}" />
					</c:url>

					<tr>
						<td>${tempPersons.firstName}</td>
						<td>${tempPersons.lastName}</td>
						<td>${tempPersons.sourceOfIncome}</td>
						<td>${tempPersons.age}</td>
						<td>${tempPersons.netWorth}</td>
						<td>${tempPersons.countryOfCitizenship}</td>
						<td>
							<a href="${updateLink}">Update</a> | <a	href="${deleteLink}" onclick="if (!(confirm('Are you sure to delete this person ?'))) return false">Delete</a>
						</td>
					</tr>
				</c:forEach>
				
			</tbody>
		</table>
	</div>

</body>
</html>