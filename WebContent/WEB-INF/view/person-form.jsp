<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Save Person</title>
	
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css" rel="stylesheet" />
	<script	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js"></script>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/add-person-style.css">
</head>
<body>

	<div class="jumbotron text-center header">
		<h2>Forbes - The World's Billionaires</h2>
	</div>
	
	<div class="jumbotron">
		<div class="center-block form-div">
			
			<div class="jumbotron text-center saving-div">
				<h3>Form Of Saving / Updating <br> a Person</h3>
			</div>
			<form:form action="savePerson" modelAttribute="person" method="POST">
		
				<form:hidden path="id" />
				<table>
					<tbody>
						<tr>
							<td><label>First name:</label></td>
							<td><form:input path="firstName" /></td>
						</tr>
						<tr>
							<td><label>Last name:</label></td>
							<td><form:input path="lastName" /></td>
						</tr>
						<tr>
							<td><label style="width: 100%">Source Of Income:</label></td>
							<td><form:input path="sourceOfIncome" /></td>
						</tr>
						<tr>
							<td><label>Age:</label></td>
							<td><form:input path="age" /></td>
						</tr>
						<tr>
							<td><label>Net Worth [in miliards]:</label></td>
							<td><form:input path="netWorth" /></td>
						</tr>
						<tr>
							<td><label>Country Of Citizenship:</label></td>
							<td><form:input path="countryOfCitizenship"  /></td>
						</tr>
						<tr>
							<td><label></label></td>
							<td><input type="submit" value="Save" class="btn btn-default" /></td>
						</tr>
					</tbody>
				</table>
			</form:form>
				
			<p><a href="${pageContext.request.contextPath}/people/list">Back to List</a></p>
		</div>
	</div>
</body>
</html>