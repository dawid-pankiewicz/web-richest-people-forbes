# WEB-RICHEST-PEOPLE-FORBES #

This application allows you to perform CRUD operations on MySQL database, using Spring and Hibernate frameworks. The application was created in an environment Spring Tool Suite 3.9.1.RELEASE using Maven framework. 
Data used in application comes from the following sources: [https://www.forbes.com/billionaires/list/#version:realtime](https://www.forbes.com/billionaires/list/#version:realtime), 
and [https://www.forbes.pl/100-najbogatszych-polska/2017](https://www.forbes.pl/100-najbogatszych-polska/2017). CRUD operations type can be done using a web interface, and using the REST API. 

### The application uses: ###

* Current version Spring framework (5.0.2.RELEASE)
* Current version Hibernate framework (5.2.12.Final)
* Current version Bootstrap framework (3.3.7) [rwd + allows you to sort rows by columns]

### App look and work ###

### Web interface ###

![Alt text](https://imgur.com/1fB2fYB.jpg)

![Alt text](https://imgur.com/1pCA1Ea.jpg)

![Alt text](https://imgur.com/E8Kvf7T.jpg)

![Alt text](https://imgur.com/gEsx9QR.jpg)

![Alt text](https://imgur.com/KNycwjl.jpg)

![Alt text](https://imgur.com/iIA9XGu.jpg)

![Alt text](https://imgur.com/s4Uujhe.jpg)

![Alt text](https://imgur.com/CpVyBS9.jpg)


### REST API ###

![Alt text](https://imgur.com/93zcaMe.jpg)

![Alt text](https://imgur.com/c7mhAkB.jpg)

![Alt text](https://imgur.com/5gb0Asv.jpg)

![Alt text](https://imgur.com/8aGPoW2.jpg)

![Alt text](https://imgur.com/YCVxSD4.jpg)
